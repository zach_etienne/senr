The latest SENR/NRPy+ code may be found at
https://github.com/zachetienne/nrpytutorial
... and a gentle introduction to the (latest) code may be found here:
http://nrpyplus.net/

The original (**OUTDATED**) SENR/NRPy+ repository has been moved to
https://bitbucket.org/zach_etienne/nrpy . <- Warning: This code is deprecated and should not be used.

Please make a note of it.